# Saleforce Simple Case Time Editor

A simple Python script and GUI to allow you to quickly add time to cases without having to use the Web interface.

## Background

I love data. I like logging what I do during the day, allowing visualisation and reporting. We use Salesforce and the Lightning interface is an amazing piece of web programming - but what it is not is a lightning experience to log time against cases, which it the whole basis for our reporting. You end up with lots of tabs open onscreen, and each tab display the case number, so you end up having to hunt through the tabs to find the correct case to add time to.

My main gripe is that the time log duration has to be entered in seconds! Who works in seconds? The time logs are a custom object that work has created and added to Salesforce, so there is no reason this could not have been a more friendly unit of time.

In the previous Classic Salesforce I implemented a Chrome Extension that added Javascript listeners to the time log page that allowed you to enter time in whatever units you wanted to use. Problem is that the Lightning interface loads everything as Ajax calls and dynamically assigns the HTML elements a different ID each time they are loaded, making the process of adding listeners to the HTML way too difficult. It also does not solve the problem of having lots of tabs open...

What Salesforce does have is a nice API that allows you to do pretty much anything you can do in the web interface programatically.

This is where this tool comes in...

![Screenshot](screenshots/TimelogPrototype1.png)

## Salesforce Simple Case Time Editor

Original I thought this would just have a command line interface, but could not quite figure out how I would implement it so that it was quick and easy to use - it just felt like it would be as clunky as the web interface.

I then considered making it a Flask application and giving it a web GUI like I did for the Oce Log Reader application I wrote, but this seemed like it would be overkill and take way too long. Having never written a GUI using Python I had a quick poke around at some of the available GUI layers. TK did initially appeal as I have used Tcl/Tk way back at Uni but ended up using DearPyGUI as it seemed like the quickest way to get something up and running.

To be honest I am never going to recover the time I spent writing the GUI in the time savings I will make using it - but I will at least enjoy the time logging process (which I despised when we switched from Classic to Lightning)

Because the timelog is a custom object, this tool is unlikely to be that useful to someone outside of the business that I work for... 

## Installation

The DearPyGUI requires Python 3.6 and above. There is a Pipfile with the dependencies if you are using `pipenv`, there is a `requirements.txt` if you are using pip.

Sorry, personal project so no nice installer. I am using on Mac OSX 10.15 and using an Automator app to call the scripts directly so I have a button in the Dock to start the GUI.

## Configuration

Rename the `preferences-sample.py` to `preferences.py` and edit to include your login details and Salesforce Organisation ID.

There are controls for the colour theme, scaling of the UI and the default sort order.

## Using

Once connected, click on the case you want to add time to, fill in the required details and press 'Add Timelog'

![Screenshot](screenshots/TimelogAnim.gif)

Thats it.