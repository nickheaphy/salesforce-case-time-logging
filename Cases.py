import distutils
import preferences
import pprint
import random

# Randomise Case Names (for screenshots)
randnames = False

class Cases:
    def __init__(self):
        self.cases = []
        self.sortby = int(preferences.sortby)
        self.sortdirection = int(preferences.sortdirection)

    def additem(self, customer, subject, id, date, disabletransferautomation=False):
        '''Add item to case list'''
        if isinstance(disabletransferautomation, str):
            disabletransferautomation = bool(distutils.util.strtobool(disabletransferautomation))
        
        if randnames:
            l = list(customer)
            random.shuffle(l)
            customer = ''.join(l)
            l = list(subject)
            random.shuffle(l)
            subject = ''.join(l)

        self.cases.append({
            "customer": customer,
            "subject": subject,
            "id": id,
            "date": date,
            "disable_transfer_automation": disabletransferautomation
        })

    def _sort_subject(self):
        '''Sort the case list by subject'''
        self.cases = sorted(self.cases, key = lambda i: i['subject'].lower(), reverse=self.sortdirection)

    def _sort_customer(self):
        '''Sort the case list by customer'''
        self.cases = sorted(self.cases, key = lambda i: i['customer'].lower(), reverse=self.sortdirection)

    def _sort_date(self):
        '''Sort the case list by date'''
        self.cases = sorted(self.cases, key = lambda i: i['date'], reverse=self.sortdirection)

    def sortcases(self):
        if self.sortby == 0:
            self._sort_customer()
        elif self.sortby == 1:
            self._sort_subject()
        else:
            self._sort_date()

    def maxlength_subject(self):
        '''Return the max length of the subject'''
        maxlen = 0
        for case in self.cases:
            if len(case['subject']) > maxlen:
                maxlen = len(case['subject'])
        return maxlen

    def maxlength_customer(self):
        '''Return the max length of the customer'''
        maxlen = 0
        for case in self.cases:
            if len(case['customer']) > maxlen:
                maxlen = len(case['customer'])
        return maxlen

    def deleteall(self):
        self.cases = []

    def istransferautomationdisabled(self, caseid):
        '''Check to see if transfer automation is disabled'''
        case = next(item for item in self.cases if item["id"] == caseid)
        return case["disable_transfer_automation"]