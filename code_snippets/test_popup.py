from dearpygui.core import *
from dearpygui.simple import *

def try_login(sender, data):
    log_debug(get_value("User Name"))
    log_debug(get_value("Password"))
    close_popup(item="Login Popup")

with window("Main"):
    add_button("Login")
    with popup("Login", 'Login Popup', modal=True, mousebutton=core.mvMouseButton_Left):
        add_input_text("User Name")
        add_input_text("Password", password=True)
        add_button("Submit", callback=try_login)

    add_button('Another button')

start_dearpygui(primary_window="Main")