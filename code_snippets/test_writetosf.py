import credentials
from simple_salesforce import Salesforce, SFType
import pprint
import datetime

testcase = "5002j00000MEn8CAAT"
thedate = datetime.datetime.now().isoformat() + "Z"

# -------------------------------------
def get_salesforce_userid():
    '''Return the user ID and name of the logged in user'''
    thesql = f"SELECT ID, Name, ContactId, Email FROM User WHERE Username = '{credentials.username}' LIMIT 1"
    data = sf.query_all(thesql)
    if len(data) > 0:
        return data['records'][0]['Id'], data['records'][0]['Name']
    else:
        return None, None


sf = Salesforce(username=credentials.username, password=credentials.password, security_token=credentials.token)
sfuserid, sfusername = get_salesforce_userid()

session_time__c = SFType('Session_Time__c', sf.session_id, sf.sf_instance)
data = {
    'Case__c': testcase,
    'Type__c': 'Manual',
    'Comments__c': 'This is a test',
    'Date__c': thedate,
    'Duration__c': "1",
    'Type__c': 'Manual',
    'Agent__c': sfuserid
}

response = session_time__c.create(data)

print(response)