import datetime
from gui2 import time_convert

date1 = "2020-11-15T20:14:49.000+0000"
date2 = "2021-03-12T04:02:06.000+0000"
date3 = "2021-03-11T01:54:27.000+0000"

print(datetime.datetime.strptime(date1, "%Y-%m-%dT%H:%M:%S.%f%z"))
print(datetime.datetime.strptime(date2, "%Y-%m-%dT%H:%M:%S.%f%z"))
print(datetime.datetime.strptime(date3, "%Y-%m-%dT%H:%M:%S.%f%z"))

print(datetime.datetime.now(datetime.timezone.utc).astimezone().isoformat())

dt = datetime.datetime(
        # Note bug in add_date_picker needing -100
        year = 2021,
        month = 3,
        day = 23,
        hour = 17,
        minute = 35,
        second  = 0
    )
print(dt)
print(dt.astimezone().isoformat())

timestring= "10m1s"
print(time_convert(timestring))

timestring = "10min1sec"
print(time_convert(timestring))

timestring = "10.5m"
print(time_convert(timestring))