import preferences
from simple_salesforce import Salesforce
import pprint


sf = Salesforce(username=credentials.username, password=credentials.password, security_token=credentials.token)

myid = "0050o00000XwStLAAV"

# List all my open cases
#thesql = f"SELECT Id, Subject, CreatedDate, OwnerId FROM Case WHERE IsClosed = false AND OwnerId = '{credentials.myid}' ORDER BY CreatedDate DESC"
thesql = f"SELECT Id, Subject, CreatedDate, OwnerId, Account.Name, Disable_Transfer_Automation__c FROM Case WHERE IsClosed = false AND OwnerId = '{myid}' ORDER BY CreatedDate"

# Case ID with time logged
caseid = "5002j00000MEn8CAAT"

#thesql = f"SELECT Agent__r.Name, Agent__c, Date__c, Duration__c, Type__c, Comments__c, Case__c FROM Session_Time__c WHERE Case__r.Id = '{caseid}' ORDER BY CreatedDate DESC"
thesql = f"SELECT Case__c, Comments__c, CreatedById, CurrencyIsoCode, Date__c, Duration__c, LastModifiedById, Name, Type__c, Agent__c FROM Session_Time__c WHERE Case__r.Id = '{caseid}' ORDER BY CreatedDate DESC"


#thesql = f"SELECT ID, Name, ContactId, Email FROM User WHERE Username = '{credentials.username}' LIMIT 1"

data = sf.query_all_iter(thesql)

for row in data:
  pprint.pprint(row)
  print()

