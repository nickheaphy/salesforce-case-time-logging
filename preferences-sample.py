# Salesforce Username and Password
username = "joeblogs@sample.com"
password = "abcdefghij"

# Salesforce Security Token
# https://help.salesforce.com/articleView?id=sf.user_security_token.htm&type=5
token = "12345678"

# Salesforce Orginisation ID
# https://help.salesforce.com/articleView?id=000325251&type=1&mode=1
orgid = "12345678"

# User Interface Theme
# Valid choices are "Dark", "Light", "Classic", Dark 2", "Grey", "Dark Grey"
# "Cherry", "Purple", "Gold", "Red"
uitheme = "Dark"

# UI Scaling
# Increase this to enlarge
uiscaling = 1.0

# This is a list of the default options in the dropdown
# Add additonal if required
commentoptions = ["",
    "Prep:",
    "Training",
    "Travel to/from customer",
    "PD:"
]

# Default Sort Order
# Customer=0, Subject=1, Date=2
sortby=0

# Default Sort Direction
# Postive=0, Negative=1
sortdirection=0