# This is the GUI code for the Salesforce Case Time Logging Tool

from dearpygui import core, simple
import pyperclip
from datetime import datetime
import random
import string
import re
import webbrowser
from Cases import Cases
import preferences
from simple_salesforce import Salesforce, SFType
import pprint

# Window Defaults
core.set_main_window_size(1000,800)
core.set_global_font_scale(preferences.uiscaling)
core.set_theme(preferences.uitheme)


# -------------------------------------
# TESTING DUMMY CASES
# -------------------------------------

# for i in range(20):
#     #cases.additem(customer="".join( [random.choice(string.ascii_letters) for i in range(random.randint(4, 15))] ),
#     cases.additem(customer=f"Customer Name {i}",
#     subject=f"Case Subject {i}",
#     id=f"ABCD_{i}",
#     date="2021-03-22"
#     )

# cases.sort_customer(core.get_value("SortDirection"))


# -------------------------------------
def connect_to_salesforce(sender, data):
    '''Connect to Salesforce - sets the global variables'''
    global sf, sfuserid, sfusername # Lazy
    sf = Salesforce(username=preferences.username, password=preferences.password, security_token=preferences.token)
    sfuserid, sfusername = get_salesforce_userid()
    #core.delete_item("InitConnection")
    core.set_value("InitConnection", f"Connected as {sfusername} - {sfuserid}")
    refresh_display(None, None)

# -------------------------------------
def get_salesforce_userid():
    '''Return the user ID and name of the logged in user'''
    thesql = f"SELECT ID, Name, ContactId, Email FROM User WHERE Username = '{preferences.username}' LIMIT 1"
    data = sf.query_all(thesql)
    if len(data) > 0:
        core.log(message=f"Name: {data['records'][0]['Name']}")
        core.log(message=f"ID: {data['records'][0]['Id']}")
        return data['records'][0]['Id'], data['records'][0]['Name']
    else:
        return None, None

# -------------------------------------
def get_salesforce_cases():
    '''Get all the salesforces cases'''
    thesql = f"SELECT Id, Subject, CreatedDate, OwnerId, Account.Name, Disable_Transfer_Automation__c FROM Case WHERE IsClosed = false AND OwnerId = '{sfuserid}' ORDER BY CreatedDate DESC"
    data = sf.query_all_iter(thesql)
    cases.deleteall()

    for row in data:
        cases.additem(
            customer = row['Account']['Name'],
            subject = row['Subject'],
            id = row['Id'],
            date = datetime.strptime(row['CreatedDate'], "%Y-%m-%dT%H:%M:%S.%f%z"),
            disabletransferautomation=row['Disable_Transfer_Automation__c'] 
        )
    
    cases.sortcases()

# -------------------------------------
def time_convert(timestring):
    '''Convert the timestring into seconds. Timestring something like 1h30m10s = 5410'''
    #Remove any spaces
    totalsecs = 0.0
    timestring.strip().replace(" ","")
    the_times = list(filter(lambda item: item, re.split('[a-zA-Z]+', timestring)))
    the_units = list(filter(lambda item: item, re.split('[\d\.]+', timestring)))
    print(f"Units: {the_units}")
    print(f"Times: {the_times}")
    if len(the_times) == len(the_units):
        for i, unit in enumerate(the_units):
            print(f"Current Unit: {unit[0]}")
            if unit[0] == "h":
                totalsecs += float(the_times[i]) * 60 * 60
            if unit[0] == "s":
                totalsecs += float(the_times[i])
            if unit[0] == "m":
                totalsecs += float(the_times[i]) * 60
            if unit[0] == "q":
                totalsecs += float(the_times[i]) * 15 * 60
    #print(f"Total: {int(round(totalsecs))}")
    return int(round(totalsecs))

# -------------------------------------
def sort_display(sender,data):
    '''User has pressed the sort button'''
    cases.sortby = int(core.get_value("SortOrder"))
    cases.sortdirection = int(core.get_value("SortDirection"))
    cases.sortcases()
    # redisplay
    populate_cases()

# -------------------------------------
def open_browser_case(sender, data):
    '''User has pressed the Open Case button'''
    id = sender.split('##')[1]
    webbrowser.open_new_tab(f"https://{sf.sf_instance}/{id}")


# -------------------------------------
def delete_display(sender, data):
    '''Delete all the cases from the display'''
    core.delete_item("case_group")

# -------------------------------------
def validatetime(sender, data):
    '''Check the time is OK and enable the button'''
    id = sender.split('##')[1]
    if time_convert(core.get_value(sender)) > 0:
        core.configure_item(f"Add Time Log##{id}", enabled=True)
    else:
        core.configure_item(f"Add Time Log##{id}", enabled=False)

# -------------------------------------
def save_callback(sender, data):
    '''Save the timelog into Salesforce and clear display'''
    id = sender.split('##')[1]

    # Disable the button and show the message about adding
    core.configure_item(sender, enabled=False)
    core.configure_item(f"Adding2SF##{id}", show=True)

    # Pull all the data together
    duration = time_convert(core.get_value(f'Duration##{id}'))
    comment = core.get_value(f'Comment##{id}')
    comment_preset = core.get_value(f'Combo##{id}')
    dt = datetime(
        # Note bug in add_date_picker needing -100
        year = core.get_value(f'Date##{id}')['year'] - 100 + 2000,
        month = core.get_value(f'Date##{id}')['month'] + 1,
        day = core.get_value(f'Date##{id}')['month_day'],
        hour = core.get_value(f'Time##{id}')['hour'],
        minute = core.get_value(f'Time##{id}')['min'],
        second  = core.get_value(f'Time##{id}')['sec']
    )
    
    #print(f"Save Clicked {id} {duration} {dt} {comment_preset} {comment}")

    # build the data dictorary for SF
    data = {
        'Case__c': id,
        'Type__c': 'Manual',
        'Comments__c': (comment_preset + " " + comment).strip(),
        'Date__c': dt.astimezone().isoformat(),
        'Duration__c': duration,
        'Type__c': 'Manual',
        'Agent__c': sfuserid
    }
    # Now write to SF
    # Need the header otherwise SF unassigns the original case...
    headers = {'Sforce-Auto-Assign': 'False'}
    session_time__c = SFType('Session_Time__c', sf.session_id, sf.sf_instance)
    responce = session_time__c.create(data, headers)
    print(responce)
    #print(data)

    #TODO - need some error checking and logging

    # Clear the display
    # Clear the time log
    core.set_value(f'Duration##{id}',"")
    core.set_value(f'Comment##{id}',"")
    core.set_value(f'Combo##{id}',"")
    # Some basic error reporting
    if responce['success']:
        core.configure_item(f"Adding2SF##{id}", default_value="...Sending to Salesforce...")
        core.configure_item(f"Adding2SF##{id}", show=False)
    else:
        core.configure_item(f"Adding2SF##{id}", default_value=f"Panic: Something went wrong trying to add {responce['errors']}")

# -------------------------------------
def save_clipboard(sender, data):
    '''Save the salesforce reference to clipboard'''
    id = sender.split('##')[1]
    firstbit = "ref:_" + preferences.orgid + "._" + id[:5]
    lastbit = id[-5:-3] + ":ref"
    middlebit = id[-10:][:5].replace("0","")
    pyperclip.copy(firstbit+middlebit+lastbit)
    print(f"Save to Clipboard Clicked {sender} {firstbit+middlebit+lastbit}")
    #print(f"{firstbit} {middlebit} {lastbit}")

# -------------------------------------
def close_warning(sender, data):
    '''Close the Warning dialog'''
    id = sender.split('##')[1]
    core.close_popup(item=f"Warning##{id}")


# -------------------------------------
def refresh_display(sender, data):
    '''Load the cases from SF and refresh the display'''
    # Need to delete all the existing cases displayed and
    # reload them
    get_salesforce_cases()
    populate_cases()

# -------------------------------------
def populate_cases():
    '''Display the cases onscreen'''
    # clear the existing
    core.delete_item("case_group")

    with simple.group("case_group", parent="SF"):
        for case in cases.cases:
            id = case['id']
            customer = str(case['customer']).ljust(cases.maxlength_customer())
            subject = str(case['subject']).ljust(cases.maxlength_subject())
            date = case['date'].strftime("%Y-%m-%d")

            title = f"{customer}   |   {subject}   |   {date}"
            with simple.collapsing_header(title):
                core.add_spacing(count=2)
                with simple.group(f"leftgroup##{id}"):
                    now_time = {'hour': datetime.now().hour,
                        'min': datetime.now().minute,
                        'sec': datetime.now().second
                        }
                    # There is a bug in add_date_picker than requires you to
                    # add 100 to the year... v0.6.229
                    now_date = {'month_day': datetime.now().day,
                        'year': int(datetime.now().strftime('%y'))+100,
                        'month': int(datetime.now().month)-1
                        }
                    
                    core.add_date_picker(f"Date##{id}", default_value=now_date)
                    core.add_time_picker(f"Time##{id}", default_value=now_time)

                core.add_same_line(spacing=10)

                with simple.group(f"rightgroup##{id}"):
                    
                    core.add_button(f"Copy Email Reference to Clipboard##{id}", callback=save_clipboard)
                    # Is the transfer automation disabled?
                    disabledta = not cases.istransferautomationdisabled(id)
                    with simple.popup(f"Copy Email Reference to Clipboard##{id}", f'Warning##{id}', modal=True, mousebutton=core.mvMouseButton_Left, show=disabledta):
                        core.add_text(f"Warn1{id}", default_value="Note that 'Disable Transfer Automation?' is not enabled on this Case")
                        core.add_text(f"Warn2{id}", default_value="Using the reference in an email will unassign the case")
                        core.add_text(f"Warn3{id}", default_value="Please consider disabling the automation")
                        core.add_button(f"Ok##{id}", callback=close_warning)
                    core.add_same_line(spacing=10)
                    core.add_button(f"Open Case in Browser##{id}", callback=open_browser_case)

                    core.add_spacing(count=3)

                    core.add_text(f"TimeText##{id}", default_value="Time")
                    core.add_input_text(f"Duration##{id}", label="", callback=validatetime)
                    core.add_text(f"Desc##{id}", default_value="Comment Prefix")
                    core.add_combo(f"Combo##{id}", label="", items=preferences.commentoptions)
                    core.add_text(f"Desc2##{id}", default_value="Comment")
                    core.add_input_text(f"Comment##{id}", label="")

                    core.add_button(f"Add Time Log##{id}", callback=save_callback, enabled=False)
                    core.add_same_line(spacing=10)
                    core.add_text(f"Adding2SF##{id}", default_value="...Sending to Salesforce...", show=False, color=[255,0,0,255])

                    core.add_spacing(count=3)
                core.add_spacing(count=2)

# -------------------------------------
def main():
    with simple.window("SF"):

        core.set_main_window_title("Salesforce Time Logger")
        
        with simple.group("Info"):
            core.add_text("InitConnection", default_value="Connecting to Salesforce and downloading cases...")
            #, color=[255,0,0,255]
            core.add_spacing(count=4)

        with simple.group(f"Options", horizontal=True):
            core.add_text("Sort Order:")
            core.add_same_line(spacing=10)
            core.add_radio_button("SortOrder", horizontal=True, items=["Customer","Subject","Date"], callback=sort_display, default_value=preferences.sortby)
            core.add_same_line(spacing=10)
            core.add_text("   Sort Direction:")
            core.add_same_line(spacing=60)
            core.add_radio_button("SortDirection", horizontal=True, items=["+","-"], callback=sort_display, default_value=preferences.sortdirection)
            core.add_same_line(spacing=60)
            core.add_button(f"Reload from SF", callback=refresh_display)
            core.add_spacing(count=4)
            
        with simple.group("case_group"):
            pass
        #populate_cases()

    # Start the GUI
    core.set_start_callback(callback=connect_to_salesforce)
    core.start_dearpygui(primary_window="SF")


if __name__ == "__main__":
    #Globals

    # TODO - Need to check if we can connect to the network

    cases = Cases()
    #sf = Salesforce(username=preferences.username, password=preferences.password, security_token=preferences.token)
    sf = None
    sfuserid = None
    sfusername = None

    main()
